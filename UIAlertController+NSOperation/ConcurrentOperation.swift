//
//  ConcurrentOperation.swift
//  UIAlertController+NSOperation
//
//  Created by Nick Harris on 2/9/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation

class ConcurrentOperation: NSOperation {
    
    override init() {
        
        override_executing = false
        override_finished = false
        
        super.init()
    }
    
    func completeOperation() {
        executing = false
        finished = true
    }

    private var override_executing : Bool
    override var executing : Bool {
        get { return override_executing }
        set {
            willChangeValueForKey("isExecuting")
            override_executing = newValue
            didChangeValueForKey("isExecuting")
        }
    }
    
    private var override_finished : Bool
    override var finished : Bool {
        get { return override_finished }
        set {
            willChangeValueForKey("isFinished")
            override_finished = newValue
            didChangeValueForKey("isFinished")
        }
    }
}
