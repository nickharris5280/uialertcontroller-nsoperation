//
//  OperationThree.swift
//  UIAlertController+NSOperation
//
//  Created by Nick Harris on 2/9/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit

class OperationThree: ConcurrentOperation {

    override func start() {
        
        if cancelled {
            print("OperationThree - CANCELLED")
            completeOperation()
            return
        }
        
        print("OperationThree.start()")
        promptUser()
    }
    
    func promptUser() {
        
        let alertController = UIAlertController(title: "You chose Operation Three!", message: nil, preferredStyle: .Alert)
        
        let alertAction = UIAlertAction(title: "Yay!", style: .Default) {
            [unowned self]
            action in
            
            self.completeOperation()
        }
        alertController.addAction(alertAction)
        
        dispatch_async(dispatch_get_main_queue(),{
            if let appDelegate = UIApplication.sharedApplication().delegate,
                let appWindow = appDelegate.window!,
                let rootViewController = appWindow.rootViewController {
                    rootViewController.presentViewController(alertController, animated: true, completion: nil)
            }
        })
    }
    
    deinit {
        print("OperationThree.deinit")
    }
}
