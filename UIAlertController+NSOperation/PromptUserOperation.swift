//
//  PromptUserOperation.swift
//  UIAlertController+NSOperation
//
//  Created by Nick Harris on 2/9/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit

class PromptUserOperation: ConcurrentOperation {
    
    var operationResult: (error: NSError?, operationChoice: OperationChoice)
    
    override init() {
        
        operationResult = (error: nil, operationChoice: .Unknown)
        
        super.init()
    }
    
    override func start() {
        
        print("PromptUserOperation.start()")
        promptUser()
    }
    
    func promptUser() {
        
        let alertController = UIAlertController(title: "Which operation should be performed?", message: nil, preferredStyle: .ActionSheet)
        
        alertController.addAction(createOperationAlertAction(OperationChoice.OperationOne))
        alertController.addAction(createOperationAlertAction(OperationChoice.OperationTwo))
        alertController.addAction(createOperationAlertAction(OperationChoice.OperationThree))
        
        dispatch_async(dispatch_get_main_queue(),{
            if let appDelegate = UIApplication.sharedApplication().delegate,
                let appWindow = appDelegate.window!,
                let rootViewController = appWindow.rootViewController {
                    rootViewController.presentViewController(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func createOperationAlertAction(operationChoice: OperationChoice) -> UIAlertAction {
        
        return UIAlertAction(title: operationChoice.actionSheetOptionTitle(), style: .Default) {
            [unowned self]
            action in
            
            self.userSelection(operationChoice)
        }
    }

    func userSelection(selectedOperation: OperationChoice) {
        print("PromptUserOperation.userSelection( \(selectedOperation.actionSheetOptionTitle()) )")
        
        operationResult = (error: nil, operationChoice: selectedOperation)
        completeOperation()
    }
    
    deinit {
        print("PromptUserOperation.deinit")
    }
}
