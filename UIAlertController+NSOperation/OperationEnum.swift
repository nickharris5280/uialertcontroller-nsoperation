//
//  OperationEnum.swift
//  UIAlertController+NSOperation
//
//  Created by Nick Harris on 2/9/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import Foundation

enum OperationChoice {
    case Unknown
    case OperationOne
    case OperationTwo
    case OperationThree
    
    func actionSheetOptionTitle() -> String {
        switch self {
        case .Unknown: return "Unknown"
        case .OperationOne: return "Operation One"
        case .OperationTwo: return "Operation Two"
        case .OperationThree: return "Operation Three"
        }
    }
}