//
//  ViewController.swift
//  UIAlertController+NSOperation
//
//  Created by Nick Harris on 2/9/16.
//  Copyright © 2016 Nick Harris. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func runOperations() {
        
        // create an operation queue
        let operationQueue = NSOperationQueue()
        
        // create the operations
        let promptUserOperation = PromptUserOperation()
        let operationOne = OperationOne()
        let operationTwo = OperationTwo()
        let operationThree = OperationThree()
        
        // create the process operation
        let processChoiceOperation = NSBlockOperation {
            [unowned promptUserOperation, unowned operationOne, unowned operationTwo, unowned operationThree] in
            
            print("processChoiceOperation")
            
            let promptUserOperationResult = promptUserOperation.operationResult
            
            if let error = promptUserOperationResult.error {
                print("Error while prompting the user: \(error)")
            }
            else {
                switch promptUserOperationResult.operationChoice {
                case OperationChoice.OperationOne:
                    operationTwo.cancel()
                    operationThree.cancel()
                case OperationChoice.OperationTwo:
                    operationOne.cancel()
                    operationThree.cancel()
                case OperationChoice.OperationThree:
                    operationOne.cancel()
                    operationTwo.cancel()
                default:
                    fatalError("Unknown operation choice: \(promptUserOperationResult.operationChoice)")
                }
            }
        }
        
        // set the dependencies
        processChoiceOperation.addDependency(promptUserOperation)
        operationOne.addDependency(processChoiceOperation)
        operationTwo.addDependency(processChoiceOperation)
        operationThree.addDependency(processChoiceOperation)
        
        // queue the operations
        operationQueue.addOperation(promptUserOperation)
        operationQueue.addOperation(processChoiceOperation)
        operationQueue.addOperation(operationOne)
        operationQueue.addOperation(operationTwo)
        operationQueue.addOperation(operationThree)
    }
}

